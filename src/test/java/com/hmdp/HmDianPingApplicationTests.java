package com.hmdp;

import com.hmdp.utils.RedisIdWorker;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@SpringBootTest
class HmDianPingApplicationTests {

    @Resource
    private RedisIdWorker redisIdWorker;

    private ExecutorService es = Executors.newFixedThreadPool(500);   //线程池

    @Test
    void testIdGenerator() throws InterruptedException {
        CountDownLatch countDownLatch = new CountDownLatch(300);  //阻塞当前线程

        Runnable task = ()->{
            for(int i = 0; i < 100; i++){
                long id = redisIdWorker.nextId("shop");
                System.out.println("id = " + id);
            }
            countDownLatch.countDown();    //完成一次任务，计数器-1
        };
        //执行三百次任务，共30000个id
        long begin = System.currentTimeMillis();

        for(int i = 0; i < 300;i++){
            es.submit(task);
        }
        countDownLatch.await();

        long end = System.currentTimeMillis();

        System.out.println("共耗时:"+ (end - begin));
    }

}
