package com.hmdp.config;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RedissonConfig {

    @Bean
    public RedissonClient redissonClient(){
        //1. 获取redisson的配置
        Config config = new Config();
        //2.配置redis 地址和密码
        config.useSingleServer().setAddress("redis://127.0.0.1:6379").setPassword("123456");

        //返回redissonClient对象
        return Redisson.create(config);
    }
}
