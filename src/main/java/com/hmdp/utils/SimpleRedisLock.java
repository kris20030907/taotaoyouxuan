package com.hmdp.utils;

import cn.hutool.core.lang.UUID;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.concurrent.TimeUnit;


public class SimpleRedisLock implements ILock{


    private StringRedisTemplate stringRedisTemplate;
    private String name;   //需要加锁的业务名称

    public SimpleRedisLock( String name,StringRedisTemplate stringRedisTemplate) {
        this.stringRedisTemplate = stringRedisTemplate;
        this.name = name;
    }

    private static final String KEY_PREFIX = "lock:";
    private static final String ID_PREFIX = UUID.randomUUID().toString(true) + "-";  //作为线程id前缀

    @Override
    public boolean tryLock(long timeOutSec) {
        //重新拼接线程ID
        String threadId = ID_PREFIX + Thread.currentThread().getId();
        //1.获取锁
        Boolean success = stringRedisTemplate.opsForValue()
                .setIfAbsent(KEY_PREFIX + name, threadId , timeOutSec, TimeUnit.SECONDS);
        return Boolean.TRUE.equals(success);
    }

    //释放锁
    @Override
    public void unlock() {
        //获取线程标识
        String threadId = ID_PREFIX + Thread.currentThread().getId();
        //获取 redis 锁的标识
        String id = stringRedisTemplate.opsForValue().get(KEY_PREFIX + name);
        //判断是否一致，如果不一致则说明当前线程无权进行该锁的删除,一致则可以删
        if(threadId.equals(id)) {
            //这里可能会发生堵塞,由于JVM的GC垃圾回收机制
            stringRedisTemplate.delete(KEY_PREFIX+name);
        }
    }
}
