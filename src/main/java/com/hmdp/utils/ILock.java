package com.hmdp.utils;

public interface ILock {
    /**
     * 自己实现redis锁的接口
     * timeOutSec是过期时间
     * 注意：这里要实现的是非阻塞式的锁，否则会影响性能
     */
    boolean tryLock(long timeOutSec);

    //释放锁
    void unlock();
}
