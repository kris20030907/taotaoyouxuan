package com.hmdp.utils;

import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

@Component
public class RedisIdWorker {

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    private final static long BEGIN_TIME = 1672531200L;  //初始时间，用于生成时间戳
    private final static int COUNT_BITS = 32;    //序列号的位数，可更改

    /**
     * 生成id，前缀由于区分不同业务
     * @param keyPrefix
     * @return
     */
    public long nextId(String keyPrefix){
        //1.生成时间戳 : 当前秒数 - 初始时间秒数
        LocalDateTime now = LocalDateTime.now();
        long nowTime = now.toEpochSecond(ZoneOffset.UTC);

        long timestamp = nowTime - BEGIN_TIME;
        //2.生成序列号  使用redis生成自增长序列号(每一天都对应一个不同的icrKey)

         //2.1 获取当前的日期，精确到天
        String day = now.format(DateTimeFormatter.ofPattern("yyyyMMdd"));

         //2.2 redis生成自增长key的值(序列号)
        long count = stringRedisTemplate.opsForValue().increment("icr:" + keyPrefix + ":" + day);

        //3.拼接并返回,注意这里不是字符串拼接，而是位运算得到32位 long 类型的值

        return timestamp << COUNT_BITS | count ;    //使用或运算，使空出的32位填充成序列号
    }

    public static void main(String[] args) {
        //生成一个初始时间
        LocalDateTime time = LocalDateTime.of(2023, 1, 1, 0, 0, 0);
        long second = time.toEpochSecond(ZoneOffset.UTC);
        System.out.println(second);

    }
}
