package com.hmdp.dto;

import lombok.Data;

import java.util.List;

/**
 * redis滚动分页查询的返回值
 */
@Data
public class ScrollResult {
    private List<?> list;   //小于指定时间戳的笔记集合
    private Long minTime;  //本次查询推送时间中最小的时间戳，用于下次查询
    private Integer offset;   //集合中相同时间戳的元素个数作为偏移量，防止查询到重复信息
}
