package com.hmdp.service.impl;

import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.hmdp.entity.ShopType;
import com.hmdp.mapper.ShopTypeMapper;
import com.hmdp.service.IShopTypeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author 虎哥
 * @since 2021-12-22
 */
@Service
public class ShopTypeServiceImpl extends ServiceImpl<ShopTypeMapper, ShopType> implements IShopTypeService {

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 使用redis存储shopType集合
     */
    public List<ShopType> listShopType() {
        //这里就不设置常量了,有需要自己可以换一下
        String key = "cache:shop-type:";  //后来注意到，这个缓存的key必须一致，不然就没意义了

        //1.判断redis中是否存在该list集合
        String shopTypeJson = stringRedisTemplate.opsForValue().get(key);

        //2.存在则直接返回解析后的shopType
        if (StrUtil.isNotBlank(shopTypeJson)) {
            //将json字符串转为存储ShopType的List集合
            return JSONUtil.toList(shopTypeJson, ShopType.class);
        }

        //3.不存在，需要查询数据库进行返回，且要存入缓存中
        List<ShopType> list = list();

        if (list != null && list.size() > 0) {
            //数据存在，json化后存redis中
            stringRedisTemplate.opsForValue().set(key, JSONUtil.toJsonStr(list));
        }

        return list;
    }
}
