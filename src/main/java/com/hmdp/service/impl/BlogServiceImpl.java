package com.hmdp.service.impl;

import cn.hutool.core.util.BooleanUtil;
import cn.hutool.core.util.StrUtil;
import com.hmdp.dto.Result;
import com.hmdp.dto.ScrollResult;
import com.hmdp.dto.UserDTO;
import com.hmdp.entity.Blog;
import com.hmdp.entity.Follow;
import com.hmdp.entity.User;
import com.hmdp.mapper.BlogMapper;
import com.hmdp.mapper.UserMapper;
import com.hmdp.service.IBlogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hmdp.service.IFollowService;
import com.hmdp.utils.UserHolder;
import jodd.util.StringUtil;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;


@Service
public class BlogServiceImpl extends ServiceImpl<BlogMapper, Blog> implements IBlogService {

    @Resource
    private UserMapper userMapper;

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Resource
    private IFollowService followService;
    /**
     * 查看博客详情
     * @param id
     * @return
     */
    @Override
    public Blog blogDetail(Long id) {
        //根据blog查信息，再根据user_id查用户信息
        Blog blog = getById(id);

        Long userId = blog.getUserId();

        User user = userMapper.selectById(userId);

        blog.setIcon(user.getIcon());
        blog.setName(user.getNickName());

        return blog;
    }

    /**
     * 获取blog点赞数
     * @param id
     * @return
     */
    @Override
    public Integer getLikes(Long id) {
        Blog blog = getById(id);
        return blog.getLiked();
    }

    /**
     * 点赞blog
     * @param id
     * @return
     */
    @Override
    public Result likeBlog(Long id) {
        //使用redis存储该博客已经存储的用户id，使用set集合存储（保证唯一性且为集合）
        //判断当前用户是否点过赞，如果点过则取消点赞属性isLike为false，反之为true


        //1.获取当前登录的用户
        Long userId = UserHolder.getUser().getId();

        //2.判断是否点过赞
        String key = "blog:liked:" + id;
        Boolean isMember = stringRedisTemplate.opsForSet().isMember(key, userId.toString());

        if(BooleanUtil.isFalse(isMember)){
            //3.如果未点赞，数据库点赞数更新 + 1，将用户id保存到set集合
            boolean updateLike = update().setSql("liked = liked + 1").eq("id", id).update();
            if(updateLike){
                //更新成功，将用户id保存到set集合
                stringRedisTemplate.opsForSet().add(key, userId.toString());
            }
        }else {
            //4.已点赞，取消点赞，数据库点赞数-1，将用户从set集合里移除
            boolean updateLike = update().setSql("liked = liked - 1").eq("id", id).update();
            if(updateLike) stringRedisTemplate.opsForSet().remove(key, userId.toString());
        }

        return Result.ok();
    }

    /**
     * 保存blog并推送给粉丝
     * @param blog
     * @return
     */
    @Override
    public Result saveBlog(Blog blog) {
        //1.只保留blog时
        UserDTO user = UserHolder.getUser();
        Long userId = user.getId();
        blog.setUserId(userId);
        boolean success = save(blog);

        if(!success) Result.fail("新增笔记失败");
        //2.推送给粉丝
            //2.1查询blog作者的粉丝
        List<Follow> follows = followService.query().eq("follow_user_id", userId).list();

        //2.2推送
        for (Follow follow : follows) {
            //获取粉丝id
            Long fan = follow.getUserId();
            //推送 存redis,使用ZSET，保证有序，且能满足“数据不断更新的特殊的分页查询”
            String key = "feed:" + fan;
            Boolean pushed = stringRedisTemplate.opsForZSet().add(key, blog.getId().toString(), System.currentTimeMillis());

        }
        return Result.ok(blog.getId());
    }

    /**
     * 查询关注的人发的博客,使用ZSET分页查询
     * @param max
     * @param offset
     * @return
     */
    @Override
    public Result queryBlogOfFollow(Long max, Integer offset) {
        //获取当前用户id
        Long userId = UserHolder.getUser().getId();

        String key = "feed:" + userId;
        //查询收件箱,redis中的博客blogId 以及发布笔记时的时间戳 minTime ,计算offset：重复时间戳的博客数量

        //滚动分页查询
        Set<ZSetOperations.TypedTuple<String>> typedTuples = stringRedisTemplate.opsForZSet()
                .reverseRangeByScoreWithScores(key, 0, max, offset, 2);

        if(typedTuples == null || typedTuples.isEmpty()){
            return Result.ok();
        }

        List<Long> ids = new ArrayList<>(typedTuples.size());
        long minTime = 0L;
        int os = 1;   //重复个数
        for (ZSetOperations.TypedTuple<String> tuple : typedTuples) {
            //获取blogId
            String id_str = tuple.getValue();
            if(id_str != null) ids.add(Long.valueOf(id_str));

            long time = tuple.getScore().longValue();
            //计算重复个数
            if(time == minTime) os++; else {
                minTime = time;
                os = 1;
            }
        }
        //根据博客id查询blog，封装list集合返回
        String idStr = StrUtil.join(",", ids);
//        List<Blog> blogs = query().in("id", ids).last("ORDER_BY FIELD(id," + idStr + ")").list();
        List<Blog> blogs = listByIds(ids);

        ScrollResult scrollResult = new ScrollResult();
        scrollResult.setList(blogs);
        scrollResult.setMinTime(minTime);
        scrollResult.setOffset(os);

        return Result.ok(scrollResult);
    }
}
